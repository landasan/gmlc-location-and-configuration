# GMLC - Location and Configuration Files #


/opt/paic/gmlc/restcomm-gmlc-3.0.0-SNAPSHOT/wildfly-10.1.0.Final/standalone/data/

-Diameter config location
/opt/paic/gmlc/restcomm-gmlc-3.0.0-SNAPSHOT/wildfly-10.1.0.Final/modules/system/layers/base/org/restcomm/diameter/lib/main/config/

## GmlcManagement_gmlcproperties.xml

<!---------------------------------------------------------------------------------------------------------------------------------------------------------------->
<?xml version="1.0" encoding="UTF-8" ?>
<gmlcgt value="60132400047"/>
<gmlcssn value="145"/>
<hlrssn value="6"/>
<mscssn value="8"/>
<vlrssn value="7"/>
<maxmapv value="3"/>
<maxactivitycount value="1000"/>
<dialogtimeout value="30000"/>
<diameteroriginrealm value="scpmloc2.net"/>
<diameteroriginhost value="kepong.scpmloc2.net"/>
<diameterdestrealm value="epc.mnc019.mcc502.3gppnetwork.org"/>
<diameterdesthost value="kpgsps01.dra.epc.mnc019.mcc502.3gppnetwork.org"/>
<diametergmlcnumber value="60132400047"/>
<lcsurlcallback value="http://localhost:8081/api/report"/>
<mongohost value="localhost"/>
<mongoport value="27017"/>
<mongodatabase value="gmlc"/>

## SCTPManagement_sctp.xml

<?xml version="1.0" encoding="UTF-8" ?>
<connectdelay value="5000"/>
<servers/>
<associations>
	<name value="GMLC01-STP01"/>
	<association name="GMLC01-STP01" assoctype="CLIENT" hostAddress="10.221.32.19" hostPort="2905" peerAddress="10.223.45.197" peerPort="4728" serverName="" ipChannelType="0" extraHostAddresseSize="0"/>
</associations>

## Mtp3UserPart_m3ua1.xml

<?xml version="1.0" encoding="UTF-8" ?>
<heartbeattime value="10000"/>
<aspFactoryList>
	<aspFactory name="ASP1-GMLC01" assocName="GMLC01-STP01" started="true" maxseqnumber="256" aspid="2" heartbeat="true"/>
</aspFactoryList>
<asList>
	<as name="AS1-GMLC01" minAspActiveForLb="1" functionality="AS" exchangeType="SE">
		<routingContext size="1">
			<rc value="0"/>
		</routingContext>
		<trafficMode mode="2"/>
		<defTrafficMode mode="2"/>
		<asps>
			<asp name="ASP1-GMLC01"/>
		</asps>
	</as>
</asList>
<route>
	<key value="12211:12277:3"/>
	<routeAs trafficModeType="2" as="AS1-GMLC01"/>
</route>

## SccpStack_sccprouter2.xml

<?xml version="1.0" encoding="UTF-8" ?>
<rule>
	<id value="1"/>
	<value ruleType="Solitary" loadSharingAlgo="Undefined" originatingType="RemoteOriginated" mask="K" paddress="1" saddress="-1" networkId="0">
		<patternSccpAddress pc="0" ssn="0">
			<ai value="16"/>
			<gt type="GT0100" tt="0" es="1" np="1" nai="4" digits="60132400047"/>
		</patternSccpAddress>
	</value>
	<id value="2"/>
	<value ruleType="Solitary" loadSharingAlgo="Undefined" originatingType="LocalOriginated" mask="K" paddress="2" saddress="-1" networkId="0">
		<patternSccpAddress pc="0" ssn="0">
			<ai value="16"/>
			<gt type="GT0100" tt="0" es="1" np="1" nai="4" digits="*"/>
		</patternSccpAddress>
	</value>
</rule>
<routingAddress>
	<id value="1"/>
	<sccpAddress pc="12277" ssn="145">
		<ai value="19"/>
		<gt type="GT0100" tt="0" es="1" np="1" nai="4" digits="60132400047"/>
	</sccpAddress>
	<id value="2"/>
	<sccpAddress pc="12211" ssn="0">
		<ai value="17"/>
		<gt type="GT0100" tt="0" es="1" np="1" nai="4" digits="-"/>
	</sccpAddress>
</routingAddress>
<longMessageRule/>
<sap>
	<id value="1"/>
	<value mtp3Id="1" opc="12277" ni="2" networkId="0">
		<mtp3DestinationMap>
			<id value="1"/>
			<value firstDpc="1" lastDpc="16384" firstSls="0" lastSls="255" slsMask="255"/>
		</mtp3DestinationMap>
	</value>
</sap>

## SccpStack_sccpresource2.xml

<?xml version="1.0" encoding="UTF-8" ?>
<remoteSsns>
	<id value="1"/>
	<value remoteSpc="12211" remoteSsn="6" remoteSsnFlag="0" markProhibitedWhenSpcResuming="false"/>
	<id value="2"/>
	<value remoteSpc="12211" remoteSsn="7" remoteSsnFlag="0" markProhibitedWhenSpcResuming="false"/>
	<id value="3"/>
	<value remoteSpc="12211" remoteSsn="8" remoteSsnFlag="0" markProhibitedWhenSpcResuming="false"/>
</remoteSsns>
<remoteSpcs>
	<id value="1"/>
	<value remoteSpc="12211" remoteSpcFlag="0" mask="0"/>
</remoteSpcs>
<concernedSpcs/>

## SccpStack_management2.xml

<?xml version="1.0" encoding="UTF-8" ?>
<zmarginxudtmessage value="240"/>
<reassemblytimerdelay value="15000"/>
<maxdatamessage value="2560"/>
<removespc value="true"/>
<previewMode value="false"/>
<sccpProtocolVersion value="ITU"/>
<ssttimerduration_min value="10000"/>
<ssttimerduration_max value="600000"/>
<ssttimerduration_increasefactor value="1.5"/>

## TcapStack_management.xml

<?xml version="1.0" encoding="UTF-8" ?>
<dialogidletimeout value="9500"/>
<invoketimeout value="9500"/>
<maxdialogs value="1000"/>
<dialogidrangestart value="1"/>
<dialogidrangeend value="2147483647"/>
<donotsendprotocolversion value="false"/>
<slsrange value="All"/>
<statisticsenabled value="false"/>

## jdiameter-config.xml

<?xml version="1.0"?>
<Configuration xmlns="http://www.jdiameter.org/jdiameter-server">

  <LocalPeer>
    <URI value="aaa://kepong.scpmloc2.net:3868" />
    <IPAddresses>
      <IPAddress value="10.221.32.19" />
    </IPAddresses>
    <Realm value="scpmloc2.net" />
    <VendorID value="0" />
    <ProductName value="Extended-GMLC" />
    <FirmwareRevision value="1" />
    <!--OverloadMonitor>
      <Entry index="1" lowThreshold="0.5" highThreshold="0.6">
        <ApplicationID>
          <VendorId value="193" />
          <AuthApplId value="0" />
          <AcctApplId value="19302" />
        </ApplicationID>
      </Entry>
    </OverloadMonitor-->
  </LocalPeer>

  <Parameters>
    <AcceptUndefinedPeer value="true" />
    <DuplicateProtection value="false" />
    <DuplicateTimer value="240000" />
    <DuplicateSize value="5000" />
    <UseUriAsFqdn value="false" /> <!-- Needed for Ericsson Emulator (set to true) -->
    <QueueSize value="10000" />
    <MessageTimeOut value="60000" />
    <StopTimeOut value="10000" />
    <CeaTimeOut value="10000" />
    <IacTimeOut value="30000" />
    <DwaTimeOut value="10000" />
    <DpaTimeOut value="5000" />
    <RecTimeOut value="10000" />

    <!-- Peer FSM Thread Count Configuration -->
    <PeerFSMThreadCount value="3" />

    <!-- Statistics Configuration -->
    <Statistics pause="30000" delay="30000" enabled="false" active_records="Concurrent,ScheduledExecService,Network,ScheduledExecService,AppGenRequestPerSecond,NetGenRequestPerSecond,Peer,Peer.local,PeerFSM"/>

    <Concurrent>
      <Entity name="ThreadGroup" size="64"/> <!-- Common Thread Pool -->
      <Entity name="ProcessingMessageTimer" size="1"/>
      <Entity name="DuplicationMessageTimer" size="1"/>
      <Entity name="RedirectMessageTimer" size="1"/>
      <Entity name="PeerOverloadTimer" size="1"/>
      <Entity name="ConnectionTimer" size="1"/>
      <Entity name="StatisticTimer" size="1"/>
    </Concurrent>

    <Dictionary enabled="false" sendLevel="MESSAGE" receiveLevel="OFF" />

    <!-- Router Request Table Cache Size and ClearSize -->
    <RequestTable size="10240" clear_size="2048" />

  </Parameters>

  <Network>
    <Peers>
      <Peer name="aaa://kpgsps01.dra.epc.mnc019.mcc502.3gppnetwork.org:6500" portRange="2906-2906" attempt_connect="true" rating="1" />
    </Peers>

    <Realms>
      <Realm name="epc.mnc019.mcc502.3gppnetwork.org" peers="kpgsps01.dra.epc.mnc019.mcc502.3gppnetwork.org" local_action="LOCAL" dynamic="true" exp_time="1">
        <ApplicationID>
          <VendorId value="10415" />
          <AuthApplId value="16777255" />
          <AcctApplId value="0" />
        </ApplicationID>
      </Realm>

      <Realm name="epc.mnc019.mcc502.3gppnetwork.org" peers="kpgsps01.dra.epc.mnc019.mcc502.3gppnetwork.org" local_action="LOCAL" dynamic="true" exp_time="1">
        <ApplicationID>
          <VendorId value="10415" />
          <AuthApplId value="16777291" />
          <AcctApplId value="0" />
        </ApplicationID>
      </Realm>

      <Realm name="epc.mnc019.mcc502.3gppnetwork.org" peers="kpgsps01.dra.epc.mnc019.mcc502.3gppnetwork.org" local_action="LOCAL" dynamic="true" exp_time="1">
        <ApplicationID>
          <VendorId value="10415" />
          <AuthApplId value="16777217" />
          <AcctApplId value="0" />
        </ApplicationID>
      </Realm>

    </Realms>
  </Network>

  <Extensions>
    <!-- To enable clustering uncomment the following lines
             <SessionDatasource value="org.mobicents.diameter.impl.ha.data.ReplicatedSessionDatasource"/>
    <TimerFacility value="org.mobicents.diameter.impl.ha.timer.ReplicatedTimerFacilityImpl"/>
    -->
    <!-- Set SCTP classes as extension points for Connection and Network Guard -->
    <Connection value="org.jdiameter.client.impl.transport.sctp.SCTPClientConnection" />
    <NetworkGuard value="org.jdiameter.server.impl.io.sctp.NetworkGuard" />
  </Extensions>

</Configuration>


